var passport = require("passport");
var passportJWT = require('passport-jwt');

require('../db/index')

var Users = require('mongoose').model('Users')
var cfg = require('../config/auth-config')

var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
    secretOrKey:cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeader()
}
module.exports = function(){
    var strategy = new Strategy(params,function(payload,done){   
        var user;
        Users.find({id: payload.id},function(err,docs){
            if(docs){
                return done(null, {
                    id:docs.id
                })
            }
            else{
                return done(new Error("User not found"), null);
            }
        })
    })
    passport.use(strategy)
    return{
        initilaize: function(){
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate('jwt', cfg.jwtSession);
        }
    }
}