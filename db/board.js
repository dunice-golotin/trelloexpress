var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Board = new Schema({
    id: String,
    name: String,
    data: [String]
});
mongoose.model('Board', Board);