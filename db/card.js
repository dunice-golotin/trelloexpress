var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Card= new Schema({
    parentId: String,
    id: String,
    value: String,
    comments: [{message: String,
                user: String}],
    description: String,
    position: Number
})
mongoose.model('Card', Card);