var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect("mongodb://localhost:27017/boarddb")
var db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", function() {
  return console.log("Connected to database at 'mongodb://localhost:27017/boarddb'" );
});
//board
require ('./board')
//list
require ('./list')
//card
require ('./card')
//user 
require ('./users')