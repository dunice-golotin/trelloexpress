var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var List = new Schema({
    parentId: String,
    id: String,
    name: String,
    value: [String],
    position: Number
})

mongoose.model('List', List);