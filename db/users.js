var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var emailExistence = require('email-existence')


var Users = new Schema({
    id: { unique: true, required: true, type: String },
    name: { unique: true, required: true, type: String },
    mail: { unique: true, required: true, type: String },
    password: { required: true, type: String },
    resetToken: String,
    resetDate: Number,
})

Users.methods.comparePassword = function (pw, cb) {
    bcrypt.compare(pw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};
Users.pre('save', function (next) {
    var user = this;
    if (!this.isModified('password') && !this.isNew) { return next(); }
    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
        }
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) {
                return next(err);
            }
            user.password = hash;
            next();
        })
    })
})

mongoose.model('Users', Users);
