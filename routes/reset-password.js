var router = require('express').Router();

var bcrypt = require('bcrypt');

var async= require('async')
var Users = require('mongoose').model('Users');

var nodemailer = require('nodemailer');
var trasporter = require('../config/mail-config');

router.post('/reset', function(req,res,next){
    async.waterfall(
        [
            function(done){
                bcrypt.genSalt(20, function(err,salt){
                    salt=salt.substr(7)
                    salt=salt.replace(/\//g,'');
                    salt=salt.replace(/\./g,'');
                    done(err,salt)
                })
            },

            function(salt, done){
                Users.findOne({mail:req.body.mail}, function(err,user){
                    if(!user){
                        return res.status(404).json({message:'user does not exist'})
                    };
                    user.resetToken=salt,
                    user.resetDate=+ new Date(),
                    user.save(function(err,doc){
                        done(err,salt,user)
                    })
                })
            },

            function(salt, user, done){
                var mailOptions = {
                    from: '<notdenis92@gmail.com>', 
                    to: user.mail, 
                    subject: 'Reset password',
                    text: `If you wanna, you can reset 
                your passport form this url http://127.0.0.1:4200/reset/${salt}. 
                Thank you for use our service ).`
                };
                trasporter.sendMail(mailOptions, function(err,info){
                    res.json({message:'message was transport at you mail'})
                    done(err,salt,user)
                })
            }
        ], 
        function(err){
            res.json({err:err});
        }
    )
})
router.post('/password', function(req,res,next){
    async.waterfall([
        function(done){
            Users.findOne({resetToken:req.body.token}, function(err,user){
                if(!user){
                    return res.json({message:'user doed not exist'})
                }
                user.password=req.body.password;
                user.resetToken=undefined;
                user.resetDate=undefined;
                user.save(function(err,doc){
                    done(err,user)
                })
            })
        },
        function(user, done){
            var mailOptions = {
                from: '<notdenis92@gmail.com>', 
                to: user.mail, 
                subject: 'Password was changed', 
                text: 'Your password was changed. Congratulation!'
            };
            trasporter.sendMail(mailOptions, function(err, info){
                res.json({message:'ok'});
            })
        }
    ])
})

module.exports = router;