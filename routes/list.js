var express = require('express');
var router = express.Router();
var async = require('async')
var auth = require('../auth/auth')()
var Card = require('mongoose').model('Card');

router.post('/add' ,function(req,res,next){
    let card = {
        parentId: req.body.boardlist.id,
        id: 'card'+(+new Date()),
        value: req.body.value,
        position: req.body.position,
        description: '',
        comments: new Array()
    }
    let newcard = new Card(card);
    newcard.save(function(err,doc){
        res.json(doc)
    })
})

router.post('/update', function(req,res,next){
    Card.findOneAndUpdate({id:req.body.id},
    {$set:{value:req.body.value}},
    function(err,doc){
    })
})

router.get('/find', auth.authenticate(), function(req,res,next){ 
    Card.find({parentId:req.query.id},
    function(err, docs){
        res.json(docs)
    })
})

router.post('/drag', function(req,res, next){
    async.waterfall([
        function(done){
            Card.find({parentId: req.body.parentId}, function(err,docs){
                for(let doc of docs){
                    doc.position=req.body.obj[doc.id]
                    doc.save(function(err,result){});
                }
                done(err,docs)
            })
        },
        function(docs,done){
            Card.findOneAndUpdate({id:req.body.id},
            {$set:{parentId:req.body.parentId, position: req.body.newposition}},
            function(err,doc){
                res.json(docs)
                done(err,docs)
            })
        },
    ])
})

module.exports = router;