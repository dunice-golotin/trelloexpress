var express = require('express');
var router  = express.Router();
var jwt = require('jwt-simple');
var cfg = require('../config/auth-config')
var Users = require('mongoose').model('Users')

var transporter = require('../config/mail-config');

router.post('/', function(req,res,next){
    
    if(!req.body.name || !req.body.password){
        return res.sends(401);
    }

    var name = req.body.name;
    var password = req.body.password;

    Users.findOne({name:name}, function(err,doc){
        
        doc.comparePassword(req.body.password, function(err, isMatch) {
            if (isMatch && !err) {
            // Create token if the password matched and no error was thrown
                var payload = {
                    id: doc.id
                };
                var token = jwt.encode(payload,cfg.jwtSecret);
                res.json({token:token});
                
            } else {
                return res.status(401).send({ success: false, message: 'Authentication failed. Passwords did not match.' });
            }
            var mailOptions = {
                from: '<notdenis92@gmail.com>', 
                to: doc.mail, 
                subject: 'Logging', 
                text: `You logged. Your token = ${token}`
            };
            transporter.sendMail(mailOptions, function(err,inf){
                
            })
        });
    });
})


module.exports = router;
