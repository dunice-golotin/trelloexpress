var express = require('express');
var router = express.Router();

var List =require('mongoose').model('List');
var Card =require('mongoose').model('Card');
var auth = require('../auth/auth')()
router.use(auth.authenticate());

router.get('/list', function(req,res,next){
    
    List.find({id:req.query.parentId}, function(err,doc){
        
        res.json(doc)
    })
})

router.post('/update', function(req,res,next){
    Card.findOneAndUpdate({id:req.body.id},
    {$set:{value:req.body.value}},function(err,doc){
        res.json(doc) 
        
    })
})

router.post('/description', function(req,res,next){
    Card.findOneAndUpdate({id:req.body.id},
    {$set:{description:req.body.description}},function(err, doc){
        res.json(doc) 
    })
})


router.post('/comment', function(req,res,next){
    Card.findOneAndUpdate({id:req.body.id},
    {$set:{comments:req.body.value}}, function(err, doc){
        res.json(doc)    
    })
})

module.exports = router;