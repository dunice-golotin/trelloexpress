var express = require('express');
var router = express.Router();
var List =require('mongoose').model('List')
var Card =require('mongoose').model('Card')
var Board = require('mongoose').model('Board')
var auth = require('../auth/auth')()
router.use(auth.authenticate());

router.get('/', function(req,res,next){
     List.find({parentId:req.query.id},function(err,docs){
        res.json(docs||{})
    })
})

router.post('/add', function(req,res,next){
   
    let newList={
        parentId:req.body.link,
        id: 'list'+(+new Date()),
        name: req.body.name,
        value: [],
        position: req.body.position
    };
    let list = new List(newList);
    list.save(function(err,doc){
        
        res.json(doc)
    })
})

router.delete('/delete', function(req,res,next){
    List.find({parentId:req.body.parentId},function(err, docs){
        for(let list of docs){
            if(list.position>req.body.position){
                list.position=list.position-1;
                list.save(function(err, doc){  
                })
            }
        }
    })
    Card.remove({parentId:req.body.id},function(err){
    })
    List.findOneAndRemove({id:req.body.id},function(err, doc){
        res.json(doc)
    })
})

router.post('/drag', function(req,res,next){
    List.find({parentId:req.body.id},function(err,docs){
        for(let doc of docs){         
            doc.position=req.body.value[doc.position]
            doc.save(function(err, result){
            });
        }
        res.json(docs)
    })
})

router.get('/link', function(req,res,next){
    Board.findOne({id:req.query.id},function(err,doc){
        res.json(doc);
    })
})

router.post('/link', function(req,res,next){
    Board.findOneAndUpdate({id:req.body.id},{$set:{name:req.body.value}},function(err,doc){
        res.json(doc)
    })
})
router.post('/listname', function(req,res,next){
    List.findOneAndUpdate({id:req.body.id},{$set:{name:req.body.name}},function(err,doc){
        res.json({name:req.body.name})
    })
})
module.exports = router;