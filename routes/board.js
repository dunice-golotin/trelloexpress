var express = require('express');
var router = express.Router();
var auth = require('../auth/auth')()
var Board = require('mongoose').model('Board')
router.use(auth.authenticate())

router.post('/' ,function(req,res,next){
    if(!req.body){
        return res.sendStatus(400)
    }
    var board = new Board({
        name: req.body.name,
        id: req.body.id,
        data: []
    })
    board.save(function(err,doc){
        res.json(doc);
    })  
})

router.get('/' ,function(req, res, next) {
    Board.find({},function(err, docs){
        res.json(docs)
    })
    /*
    Board.remove().then(function(doc){console.log(doc)});
    Card.remove().then(function(doc){console.log(doc)});
    List.remove().then(function(doc){console.log(doc)});
    */
});

router.get('/search',function(req,res,next){
    var patern = new RegExp("\\b"+req.query.id)
    Board.find({name:patern},null,{limit:5},function(err,docs){
        res.json(docs)
    })
})

module.exports = router;