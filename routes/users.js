var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer')
var trasporter = require('../config/mail-config');

var async = require('async')

var emailExistence = require('email-existence')
require('../db/index')
var Users = require('mongoose').model('Users');

router.post('/', function (req, res, next) {

    if (!req.body.name || !req.body.password || !req.body.mail) {
        return res.json({ massage: 'can not create void user' })
    }
    async.waterfall([
        function (done) {
            emailExistence.check(req.body.mail, function (err, doc) {
                if (!doc) {
                    return res.status(404).json({ message: 'mail does not exist' })
                }
                else {
                    done(err)
                }
            })
        },
        function (done) {
            var user = new Users({
                name: req.body.name,
                password: req.body.password,
                mail: req.body.mail,
                id: +(new Date())
            });
            user.save(function (err, doc) {
                if (err) {
                    return res.status(404).json({ message: 'user already exist' })
                }
                done(err)
            })
        },
        function (done) {
            var mailOptions = {
                from: '<notdenis92@gmail.com>',
                to: req.body.mail,
                subject: 'Congratulate!!',
                text: 'You are registed!'
            };
            trasporter.sendMail(mailOptions, function (err, info) {
                if (err) return done(err);
                res.json({ message: 'ok' })
            })
        }
    ]), function (err) {
        res.json({ err: err });
    }
})
router.get('/', function (req, res, next) {
    Users.find({}, function (err, doc) {
        res.json(doc)
    })
})

module.exports = router;