var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var board = require('./routes/board')
var workfield = require('./routes/workfield')
var list = require('./routes/list')
var modalWindow=require('./routes/modal-window')

var token = require('./routes/token')
var users = require('./routes/users')
var resetPassword=require('./routes/reset-password')
var auth = require('./auth/auth')()

var app = express();


// view engine setup

// uncomment after placing your favicon in /public
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(cookieParser());
app.use(cors())
app.use('/board', board);
app.use('/workfield',workfield);
app.use('/list',list)
app.use('/modal-window',modalWindow)
app.use('/token',token);
app.use('/users',users);
app.use('/reset-password',resetPassword);
// catch 404 and forward to error handler
var fake_token='fake-jwt-token';
//---------------------------------------------------
app.use(auth.initilaize());
//---------------------------------------------------*/
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json('error')
  //res.render('error');
});

module.exports = app;
